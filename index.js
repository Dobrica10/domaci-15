var App = (function(axios, Mustache) {
  var genres = [];
  var movies = [];

  var $genres = document.getElementById("genres");
  var $movies = document.getElementById("movies");
  var $movie = document.getElementById("movie");
  var $moviesdispley = document.getElementsByClassName("moviesdispley");
  var $searchmovie = document.getElementById("searchmovie");
  const $search = document.getElementById("search");

  var genresTpl = document.getElementById("genres-template").innerHTML;
  var moviesTemplate = document.getElementById("movies-template").innerHTML;
  var movieTpl = document.getElementById("movie-template").innerHTML;

  return {
    getCurrentGenre: function() {},
    getMoviesByGenre: function() {},
    start: function() {
      var self = this;
      this.attachEvents();
      this.fetch().then(function(response) {
        var data = response.data;
        genres = data.genres;
        movies = data.movies;
       
        self.createLinks();
      });
    },
    fetch: function(cb) {
      return axios.get("provera.php");
    },
    createLinks: function() {
      $genres.innerHTML = Mustache.render(genresTpl, {
        genres: genres
      });
    },

    updateMovies: function(response) {
      // TODO: compile movies template;
      var x = window.location.hash.slice(2);
      //console.log(movies);
      //console.log(genress);
      var filtermovies = [];
      for (var i = 0; i < movies.length; i++) {
        //console.log(movies[i]);
        for (var j = 0; j < movies[i].genres.length; j++) {
          if (movies[i].genres[j] == x) {
            filtermovies.push(movies[i]);
            //console.log(filtermovies);
          }
        }
      }
      //console.log(filtermovies.length);
      //console.log(x);
      console.log(filtermovies);
      $searchmovie.classList.add("pokazi");
      $movies.innerHTML = Mustache.render(moviesTemplate, {
        movies: filtermovies
      });
      //this part will list only tiped movies

      $search.addEventListener("keyup", function(e) {
        //alert("radi");
        console.log(this.value);
        console.log(filtermovies);
        const val = this.value;
        var query = "";
        var $moviesel = [];

        console.log($moviesdispley);
        for (let i = 0, len = $moviesdispley.length; i < len; i++) {
          $moviesel = $moviesdispley[i];
          //console.log(filtermovies[i]);
          query = $moviesel.getAttribute("data-query");
          //console.log(query);
          console.log($moviesel);
          if (query.toLowerCase().indexOf(val.toLowerCase()) === -1) {
            //alert("none");
            //onsole.log($moviesdispley)
            $moviesel.style.display = "none";
          } else {
            //alert("block");
            //console.log($moviesdispley)
            $moviesel.style.display = "inline-block";
          }
        }
      });

      // this part will open a new window with info from selected movie
      var oper = document.querySelectorAll(".moviesdispley");
      var len = oper.length;
      for (var r = 0; r < len; r++) {
        oper[r].addEventListener(
          "click",
          function(e) {
            var filtermovie = [];
            for (let i = 0; i < filtermovies.length; i++) {
              if (filtermovies[i].id == this.firstChild.nextSibling.innerHTML) {
                filtermovie.push(filtermovies[i]);
                console.log(filtermovie);
              }
            }
            $movie.classList.add("pokazi");
            //console.log(this.firstChild.nextSibling.innerHTML);
            //console.log(filtermovies);
            $movie.innerHTML = Mustache.render(movieTpl, {
              movies: filtermovie
            });

            // document
              // .getElementById("close-window")
              
              document.getElementById("movie").addEventListener("click", function() {
                $movie.classList.remove("pokazi");
              });
          },
          false
        );
      }
      //console.log(data)
    },

    attachEvents: function() {
      window.addEventListener("hashchange", this.updateMovies.bind(this));
    }
  };
})(axios, Mustache);

//MENJANJE POZADINE POMOCU MOUSEMOVE EVENTA
const card = document.querySelector('.wrapper');

card.addEventListener('mousemove', kakoGod);

const heading = document.getElementById('naslov');
console.log(document.getElementById('naslov'));
function kakoGod(event) {
console.log(`EVENT TYPE: ${event.type}`);

heading.textContent= `MouseX: ${event.offsetX} MouseY: ${event.offsetY}`;

document.body.style.backgroundColor = `rgb(${event.offsetX}, ${event.offsetY}, 40)`;
}
